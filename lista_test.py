import unittest
import lista_apuri

class Test_TestaaLista (unittest.TestCase):
    def testaa(self):
        lista = [1, 2,3,5,6]
        tulos = lista_apuri.parillisten_maara(lista)
        self.assertEqual(tulos, 2)

    def testi_kaksi(self):
        lista = [1, 2,3,5,6]
        tulos = lista_apuri.negatiivisten_maara(lista)
        self.assertEqual(tulos, 0)

    def testi_kolme(self):
        lista = [2,3,5, 1,6]
        tulos = lista_apuri.parittomien_maara(lista)
        self.assertEqual(tulos, 3)